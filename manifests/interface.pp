define ipoib::interface(
  $interface = $name,
  $mtu = '65520',
  $connected_mode = 'yes',
  $ipaddress = undef,
  $netmask = '255.255.255.0',
  $gateway = undef,
  $domain = undef,
  $dns1 = undef,
  $dns2 = undef,
  $arp_ignore = '1',
) {
  require ::ipoib

  if $ipaddress == undef {
    fail('ipoib::interface requires an ip address')
  }

  sysctl { "net.ipv4.conf.${interface}.arp_ignore":
    ensure => present,
    value  => $arp_ignore,
  }
  -> network::interface { $interface:
    ipaddress            => $ipaddress,
    netmask              => $netmask,
    gateway              => $gateway,
    type                 => $facts['os']['family'] ? {
      'RedHat' => 'InfiniBand',
      default => undef,
    },
    domain               => $domain,
    dns1                 => $dns1,
    dns2                 => $dns2,
    options_extra_redhat => {
      'CONNECTED_MODE' => $connected_mode,
    },
    mtu                  => $mtu,
    # Pre up for debian
    pre_up               => [
      'modprobe ib_umad',
      'modprobe mlx4_ib',
      'modprobe ib_ipoib',
      "echo connected > /sys/class/net/${interface}/mode",
    ],
  }
}
