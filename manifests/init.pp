class ipoib {
  $packages = $facts['os']['family'] ? {
    'RedHat' => ['infiniband-diags', 'libibverbs'],
    'Debian' => ['infiniband-diags', 'ibverbs-utils', 'ibverbs-providers', 'libibverbs1', 'librdmacm1'],
  }

  package { $packages:
    ensure => installed,
  }
  -> file { '/etc/modules-load.d/ipoib.conf':
    content =>
      "
mlx4_ib 
rdma_ucm
ib_umad 
ib_uverbs
ib_ipoib",
  }

  exec { 'systemctl restart systemd-modules-load':
    subscribe   => File['/etc/modules-load.d/ipoib.conf'],
    refreshonly => true,
    path    => ['/usr/bin', '/usr/sbin', '/bin',],
  }
}
